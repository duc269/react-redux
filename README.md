# useDispatch cho phép bạn gửi các actions đến store để thay đổi trạng thái của ứng dụng. Gửi action đến Redux store

-

# useSelector được cung cấp để lấy dữ liệu từ Redux store và sử dụng trong các thành phần React. Hook này cho phép bạn chọn và trích xuất các phần của trạng thái từ store Redux để sử dụng trong thành phần của bạn.

-

# configureStore được cấu hình và khởi tạo Redux store

- cấu hình các hàm middleware, reducers, và các tùy chọn khác một cách thuận tiện, giúp giảm bớt boilerplate code (code lặp đi lặp lại) và giúp việc quản lý store Redux trở nên dễ dàng hơn.

* thuộc tính reducer được sử dụng để định nghĩa rootReducer, tức là một reducer lớn chứa tất cả các reducers nhỏ của ứng dụng của bạn.

# combineReducers

- Ở đây, combineReducers được sử dụng để kết hợp các con reducers thành một rootReducer, Sau đó, rootReducer này được truyền vào trong configureStore

# createSlice giúp tạo reducers và action creators một cách dễ dàng và gọn nhẹ. Nó giúp giảm boilerplate code (code lặp đi lặp lại) khi tạo reducers và actions cho các trường hợp thay đổi trạng thái trong Redux.
