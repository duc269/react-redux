import { combineReducers } from "@reduxjs/toolkit";
import uiSlice from "./ui-slice";
import cartSlice from "./cart-slice";
import footerSlice from "./footer-slice";
const RootReducers = combineReducers({
    ui: uiSlice.reducer,
    cart: cartSlice.reducer,
    footer: footerSlice.reducer
})

export default RootReducers;