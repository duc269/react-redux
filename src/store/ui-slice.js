import { createSlice } from "@reduxjs/toolkit";


// createSlice khởi tạo ra các reducers và actions
// định nghĩa các thông tin và hành động liên quan đến state ban đầu 
const uiSlice = createSlice({
    name: 'ui',
    initialState: {cartIsVisible: false},
    reducers: {
        toggle(state) {
            state.cartIsVisible = !state.cartIsVisible;
        }
    }
});
export const uiActions = uiSlice.actions; // xuất các actions creator được tạo ra từ uiSlice

export default uiSlice;