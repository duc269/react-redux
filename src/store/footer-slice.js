import { createSlice } from "@reduxjs/toolkit";

const footerSlice = createSlice({
    name: "footer",
    initialState: { openFooter: true },
    reducers: {
        toggle(state) {
            state.openFooter = !state.openFooter;
        }
    }
});

export const footerActions = footerSlice.actions; // xuất các action của footerSlice

export default footerSlice; // xuất reducer được tạo