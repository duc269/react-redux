import { configureStore } from "@reduxjs/toolkit";
import RootReducers from "./RootReducers";
// configureStore được dùng để tạo Redux store
// reducer : chứa các reducer của ứng dụng
const store = configureStore({
  reducer: RootReducers,
});
export default store;
