import ProductItem from "./ProductItem";
import classes from "./Products.module.css";

const DUMMY_ITEMS = [
  {
    id: "p1",
    price: 6,
    title: "First Book",
    description: "The First Book",
  },
  {
    id: "p2",
    price: 5,
    title: "Second Book",
    description: "The First Book",
  },
];
const Products = (props) => {
  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        {DUMMY_ITEMS.map((item) => (
          <ProductItem
            key={item.id}
            id={item.id}
            price={item.price}
            title={item.title}
            description={item.description}
          ></ProductItem>
        ))}
      </ul>
    </section>
  );
};

export default Products;
