import { Fragment } from 'react';
import MainHeader from './MainHeader';
import MainFooter from './MainFooter';
import { useSelector } from 'react-redux';

const Layout = (props) => {
  const isOpen = useSelector((state) => state.footer.openFooter);
  return (
    <Fragment>
      <MainHeader />
      <main>{props.children}</main>
      {isOpen && <MainFooter />}
    </Fragment>
  );
};

export default Layout;
