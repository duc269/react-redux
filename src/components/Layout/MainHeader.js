import CartButton from '../Cart/CartButton';
import classes from './MainHeader.module.css';
import { useDispatch } from 'react-redux';
import { footerActions } from '../../store/footer-slice';
const MainHeader = (props) => {
  const dispatch = useDispatch();
  return (
    <header className={classes.header}>
      <h1>Redux</h1>
      <button onClick={() => dispatch(footerActions.toggle())}>Open Footer</button>
      <nav>

        <ul>
          <li>
            <CartButton />
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default MainHeader;
