import classes from './CartItem.module.css';
import { useDispatch } from 'react-redux';
import { cartActions } from '../../store/cart-slice';
const CartItem = (props) => {
  const dispatch = useDispatch();
  const onAddItemHandler = () => {
    dispatch(cartActions.addItemToCart({
      id: props.id,
      name: props.title,
      price: props.price
    }));
  }
  const onRemoveItemHandler = () => {
    dispatch(cartActions.removeItemFromCart(props.id));
  }
  


  return (
    <li className={classes.item}>
      <header>
        <h3>{props.title}</h3>
        <div className={classes.price}>
          ${props.total.toFixed(2)}{' '}
          <span className={classes.itemprice}>(${props.price.toFixed(2)}/item)</span>
        </div>
      </header>
      <div className={classes.details}>
        <div className={classes.quantity}>
          x <span>{props.quantity}</span>
        </div>
        <div className={classes.actions}>
          <button onClick={onRemoveItemHandler}>-</button>
          <button onClick={onAddItemHandler}>+</button>
        </div>
      </div>
    </li>
  );
};

export default CartItem;
