import Card from "../UI/Card";
import classes from "./Cart.module.css";
import CartItem from "./CartItem";
import { useSelector } from "react-redux";

const Cart = (props) => {
  const items = useSelector((state) => state.cart.items);
  const totalPrice = useSelector((state) => state.cart.totalPrice);
  return (
    <Card className={classes.cart}>
      <h2>Your Shopping Cart</h2>
      <h2>Total Price: ${totalPrice.toFixed(2)}</h2>
      <ul>
        {items.map((item) => (
          <CartItem
            key={item.id}
            id={item.id}
            title={item.name}
            quantity={item.quantity}
            price={item.price}
            total={item.totalPrice}
          ></CartItem>
        ))}
      </ul>
    </Card>
  );
};

export default Cart;
