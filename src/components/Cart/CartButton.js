import classes from './CartButton.module.css';
import { useDispatch } from 'react-redux';
import { uiActions } from '../../store/ui-slice';
import { useSelector } from 'react-redux';
const CartButton = (props) => {
  const dispatch = useDispatch(); // useDispatch cho phép gửi các actions tới store để thay đổi state 
  const amountItems = useSelector(state => state.cart.totalQuantity);

  const toggleCartButton = () => {
    dispatch(uiActions.toggle());
  }

  return (
    <button className={classes.button} onClick={toggleCartButton}>
      <span>My Cart</span>
      <span className={classes.badge}>{amountItems}</span>
    </button>
  );
};

export default CartButton;
